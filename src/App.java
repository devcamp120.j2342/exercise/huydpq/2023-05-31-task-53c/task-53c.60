import models.BigDog;
import models.Cat;
import models.Dog;

public class App {
    public static void main(String[] args) throws Exception {
        Cat cat = new Cat("Mèo");
        Dog dog = new Dog("Dương");
        BigDog bigDog = new BigDog("Alaska");

        cat.greets(); 
        dog.greets(); 
        dog.greets(dog);
        bigDog.greets(); 
        bigDog.greets(dog); 
        bigDog.greets(bigDog);

    }
}
